const express = require('express')
const req = require('express/lib/request')
const { json } = require('express/lib/response')
const res = require('express/lib/response')
const app = express()
const secret = "s3cr37"
const port = 5555
const cors = require('cors')
// Installer via npm : Express, Axios, Body-parser, cors et jsonwebtoken


// BDD
const users = [
    {
        "username" : "Loic",
        "password" : "loic"
    },
    {
        "username" : "Horos",
        "password" : "horos"
    },
    {
        "username" : "Makan",
        "password" : "makan"
    },
];


app.get('/', (req, res) => {
  res.send('AUTHENTIFICATION')
})

// Récupère le token pour vérifier si le client est authorisé à naviguer dans les pages.
app.post("/verification", (req, res) => {

    // reçois le token (sinon undefined) qui sera stocker dans une variable
    let tkn = (req.body.authorization !== undefined)? req.body.authorization.split(" ")[1] : undefined;

    if (tkn === undefined) res.status(403).send("Eh bah non t'as pas le droit.")
    else {
        if (jsonwebtoken.verify(tkn, secret) !== undefined) res.status(200).send("Let's go !!!")
        else res.status(403).send("Sadge...")
    }
})

// Récupère les données client transmis par l'API et créer le token avec les données. 
app.post('/authorize', (req, res) => {
    let payload = req.body;

    // Vérification des infos transmises par l'utilisateur, pour ensuite les poster à l'API
    const u = users.find(users.username === payload.usr && users.password === payload.pwd);

    if (u === undefined) res.status(403).send("UnAuthorized")

    // Renvoie le token à l'API
    else res.status(200).json({
        "token" : jsonwebtoken.sign(u, secret) // Création du Token
    });
});       

app.listen(port, () => {
  console.log(`active on port : ${port}`)
})